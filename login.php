<?php
error_reporting(E_ALL & ~E_NOTICE);
session_start();
require_once 'userFunc.php';
if(isLoggedOn()){
    header('Location: .');
    return;
}
$errno = 0;
include_once 'config.php';
if(isset($_POST['uname']) && isset($_POST['pass'])){
    //verify the data
    $sql = 'SELECT * FROM pb_user WHERE uname=?;';
    $stmt = $dbh->prepare($sql);
    $stmt->execute(array($_POST['uname']));
    $array = $stmt->fetch();
    
    if($array != false){
        if(password_verify($_POST['pass'],$array[2])){
            $_SESSION['pb_uid']=$array[0];
            header('Location: .');
            return;
        }else{
            $errno = 1;
        }
    }else{
        $errno = 1;
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Leo's Pasting Service</title>
<?php include 'stdhead.php';?>
</head>
<body>
<header>
<?php include 'header.php';?>
</header>
<div class="main">
<div class="code">
<form action="login.php" method="post">
<table>
<?php if($errno == 1){ ?>
<tr><td colspan="2">Incorrect username/password</td></tr>
<?php } ?>
<tr><th>User name</th><td><input name="uname" type="text"></td></tr>
<tr><th>Password</th><td><input name="pass" type="password"></td></tr>
<tr><td colspan="2"><input type="submit" value="Login" name=login"></td></tr>
</table>
</form>
</div>

</div>
<footer>
<?php include 'footer.php';?>
</footer>
</body>
</html>
