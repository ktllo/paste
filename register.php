<?php
require_once 'lib/recaptcha/recaptchalib.php';
include_once 'config.php';
?>
<!DOCTYPE html>
<html>
<head>
<title>Leo's Pasting Service</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<header>
<?php include 'header.php';?>
</header>
<div class="main">
<div class="code">
<form action="doRegister.php" method="post">
<table>
<tr><th>User name</th><td><input name="uname" type="text"></td></tr>
<tr><th>Password</th><td><input name="pass" type="password"></td></tr>
<tr><th>Retype Password</th><td><input name="rpass" type="password"></td></tr>
<tr><td colspan="2"><?php echo recaptcha_get_html(RECAPTCHA_PUBLIC_KEY); ?></td></tr>
<tr><td colspan="2"><input type="submit" value="Register" name="register"></td></tr>
</table>
</form>
</div>

</div>
<footer>
<?php include 'footer.php';?>
</footer>
</body>
</html>
