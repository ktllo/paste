<?php
//error_reporting(E_ALL & ~E_NOTICE);
require_once 'userFunc.php';
require_once 'lib/recaptcha/recaptchalib.php';
include_once 'config.php';
if(!isLoggedOn()){
    header('Location: .');
    return;
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Leo's Pasting Service</title>
<?php include 'stdhead.php';?>
<script>
$(document).ready(function(){
    $("#submit").click(function(){
        $.post("doNewPaste.php",$("#mainForm").serialize(),function(data,status){
            if(status == "success"){
                code = $(data).find("code").first().text();
                if( code == 401 ){
                    window.location.href = './';
                }else if( code == 200 ){
                    dest = $(data).find("data").first().text();
                    window.location.href = dest;
                }else if( code == 402 ){
                    dest = $(data).find("data").first().text();
                    alert('reCAPTCHA verify fail('+dest+')');
                    Recaptcha.reload();
                }else if( code == 404 ){
                    alert('Sorry, no quota left.');
                }
            }else{
                alert("Error : Cannot submit paste.\nAJAX request status:"+status);
            }
        });
    });
}); 


$(function() {
    $( "#inputArea" ).accordion({
        collapsible: true,
            heightStyle: "content"
    });
    $( "#submit" ).button();
});
</script>
</head>
<body>
<header>
<?php include 'header.php';?>
</header>
<div class="main">
<div class="code">
<form action="doNewPaste.php" method="post" id="mainForm">
<div id="inputArea">

<h3>Basic Information</h3>
<div>
<table>
<tr><th>Title</th><td><input type="text" name="title"></td></tr>
<tr><td colspan="2">
<textarea name="paste" rows="15" cols="80">
</textarea>
</td></tr>
</table>
</div>
<h3>Highlight and other setting</h3>
<div>
<table>
<tr><th>Highlight</th><td>
<select name="highlight">
<option value="None">Plain</option>
<?php include 'highlight.part';?>
</select>
</td></tr>
</table>
</div>
<h3>reCAPTCHA</h3>
<div>
<table>
<tr><td>&nbsp;</td><td>
<?php
if(isset($_GET['rerr']))
    echo recaptcha_get_html(RECAPTCHA_PUBLIC_KEY,$_GET['rerr']);
else
    echo recaptcha_get_html(RECAPTCHA_PUBLIC_KEY); ?>
</td></tr>
</table>
</div>

</div>
</form>
<input type="button" name="submit" value="Submit" id="submit">
</div>

</div>
<footer>
<?php include 'footer.php';?>
</footer>
</body>
</html>
