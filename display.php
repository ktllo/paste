<?php
if (empty($_GET['id'])){
    header('Location: .');
    return;
}
require_once 'lib/GeSHi/geshi.php';
include_once 'config.php';
$sql = "SELECT title,content,highlight,uname,created FROM pb_data,pb_user WHERE pid=? AND own=uid;";
$stmt = $dbh->prepare($sql);
$stmt->execute(array($_GET['id']));
$array = $stmt->fetch();
if($array == false){
    header('Location: .');
    return;
}
//$array = $result->fetch_array(MYSQLI_ASSOC);
$geshi = new GeSHi($array['content'],$array['highlight']);
$geshi->enable_line_numbers(GESHI_NO_LINE_NUMBERS);
$geshi->set_header_type(GESHI_HEADER_PRE_VALID);
//$geshi->set_line_style('background: #fcfcfc;', 'background: #f0f0f0;');
$geshi->enable_classes();
?>
<!DOCTYPE html>
<html>
<head>
<title>Leo's Pasting Service</title>
<?php include 'stdhead.php';?>
<style>
<?php echo $geshi->get_stylesheet(); ?>
</style>
</head>
<body>
<header>
<?php include 'header.php';?>
</header>
<div class="main">
<h1 class="title"><?php echo $array['title'];?></h1>
<p class="info">Created by <?php echo $array['uname'];?> On <?php echo $array['created'];?></p>
<div class="code">
<?php echo $geshi->parse_code();?>
</div>

</div>
<footer>
<?php include 'footer.php';?>
</footer>
</body>
</html>

