<?php
require_once 'config.php';
require_once 'lib/recaptcha/recaptchalib.php';
//Check inputs
if(!isset($_POST['uname']) || !isset($_POST['pass']) || !isset($_POST['rpass']) || empty($_POST["recaptcha_response_field"]) ){
    header('Location: register.php');
    return;
}

if( $_POST['pass'] != $_POST['rpass'] ){
    header('Location: register.php?err=0');
    return;
}
$resp = recaptcha_check_answer (RECAPTCHA_PRIVATE_KEY,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);
if (!$resp->is_valid){
    header('Location: register.php?err=2');
    return;
}
$sql = "SELECT * FROM `pb_user` WHERE uname=?;";
$stmt = $dbh->prepare($sql);
$stmt->execute(array($_POST['uname']));
if( $stmt->fetch() != false ){
    header('Location: register.php?err=1');
    return;
}

$hash = password_hash($_POST['pass'], PASSWORD_DEFAULT);
echo $hash;
$sql = 'INSERT INTO pb_user VALUES (NULL,?,?,100);';
$stmt = $dbh->prepare($sql);
$stmt->execute(array($_POST['uname'],$hash));
echo $link->error;
session_start();
$_SESSION['pb_uid'] = $dbh->lastInsertId();
header('Location: .');
