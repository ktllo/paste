<?php

function isLoggedOn(){
    if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
    if(empty($_SESSION['pb_uid']))
        return false;
    return $_SESSION['pb_uid'];
}
