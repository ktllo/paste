<?php echo '<?xml version="1.0"?>'?>
<?php
function base64url_encode($data) {
  return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
} 
session_start();
require_once 'userFunc.php';
require_once 'lib/recaptcha/recaptchalib.php';
header('Content-type: text/xml');
if(!isLoggedOn()){?>
<result>
    <code>401</code>
</result>
<?php 
return;
}
if(!isset($_POST['title']) || !isset($_POST['paste']) || !isset($_POST['highlight'])){
    ?>
<result>
    <code>403</code>
</result>
<?php 
return;
    return;
}
if($_POST['title'] == "") $_POST['title'] = "Untitled";
include_once 'config.php';
if(empty($_SESSION['pb_lastreCAPTCHA'])) $_SESSION['pb_lastreCAPTCHA'] = "";
//Verify reCAPTCHA
$resp = recaptcha_check_answer (RECAPTCHA_PRIVATE_KEY,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);
if (!$resp->is_valid && $_SESSION['pb_lastreCAPTCHA']!=$_POST["recaptcha_challenge_field"] ){?>
<result>
    <code>402</code>
    <data><?php echo $resp->error;?></data>
</result>
<?php 
return;
}else $_SESSION['pb_lastreCAPTCHA'] = $_POST["recaptcha_challenge_field"];

//Check Quota
$sql='SELECT max_cnt FROM pb_user WHERE uid=?';
$stmt=$dbh->prepare($sql);
$stmt->execute(array($_SESSION['pb_uid']));
$array=$stmt->fetch();
$quota = $array[0];
$sql='SELECT COUNT(*) FROM pb_data WHERE own=?';
$stmt=$dbh->prepare($sql);
$stmt->execute(array($_SESSION['pb_uid']));
$array=$stmt->fetch();
$used = $array[0];
if($used++ >= $quota){?>
<result>
    <code>404</code>
</result>
<?php   return;
}
//Generate ID
$ok=false;
while(!$ok){
    $id =  substr(base64url_encode(openssl_random_pseudo_bytes(40)),0,8);
    $sql = 'SELECT * FROM pb_data WHERE pid=?';
    $stmt = $dbh->prepare($sql);
    $stmt->execute(array($id));
    if($stmt->fetch() == false) $ok = true; 
}
$sql = 'INSERT INTO pb_data VALUES(?,?,?,0,?,NOW(),?,0)';
$stmt = $dbh->prepare($sql);
$stmt->execute(array($id,$_POST['title'],$_POST['paste'],$_POST['highlight'],$_SESSION['pb_uid']));
#echo 'OK'.$id;
?>
<result>
    <code>200</code>
    <data><?php echo $id;?></data>
</result>
