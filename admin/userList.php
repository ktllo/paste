<!DOCTYPE html>
<html>
<head>
<title>Leo's Pasting Service</title>
<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<?php include '../config.php'; ?>
<?php
$sql = 'SELECT uid,uname,max_cnt FROM pb_user;';
$stmt = $dbh->prepare($sql);
$stmt->execute(); ?>
<h1>User List</h1>
<table>
    <tr>
        <th>User ID</th>
        <th>User Name</th>
        <th>Max Paste</th>
    </tr>
<?php while($array = $stmt->fetch()) { ?>
    <tr>
        <td><?php echo $array['uid'];?></td>
        <td><?php echo $array['uname'];?></td>
        <td><?php echo $array['max_cnt'];?></td>
    </tr>
<?php } ?>
</body>
</html>
