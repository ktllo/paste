<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="jquery-ui.css">
<script src="jquery-2.1.1.js"></script>
<script src="jquery-ui.js"></script>
<?php
require_once 'userFunc.php';
if(isLoggedOn()){
include_once 'config.php';

$sql='SELECT max_cnt FROM pb_user WHERE uid=?';
$stmt=$dbh->prepare($sql);
$stmt->execute(array($_SESSION['pb_uid']));
$tarray=$stmt->fetch();
define('QUOTA_ASSIGNED',$tarray[0]);
$sql='SELECT COUNT(*) FROM pb_data WHERE own=?';
$stmt=$dbh->prepare($sql);
$stmt->execute(array($_SESSION['pb_uid']));
$tarray=$stmt->fetch();
define('QUOTA_USED',$tarray[0]);
?>
<script>
$(function() {
    $("#pbQuota").progressbar({ value: <?php echo QUOTA_USED;?>, max: <?php echo QUOTA_ASSIGNED;?>});
});
</script>
<?php
}
